import java.util.Scanner;

/**
 *@Author Ivan J. Miranda
 *STATUS: SOLVED! 
 *
 *=STRANGE GRID=
 * 
 *Problem Statement:
 *A strange grid has been recovered from an old book. It has 5 columns and infinite number of rows. 
 *The bottom row is considered as the first row. First few rows of the grid are like this:
 *
 *..............
 *..............
 *20 22 24 26 28
 *11 13 15 17 19
 *10 12 14 16 18
 * 1  3  5  7  9
 * 0  2  4  6  8
 *The grid grows upwards forever!
 *Your task is to find the integer in c th column in r th row of the grid.
 *
 *Input Format:
 *There will be two integers r and c separated by a single space. 
 *
 *Constraints: 
 *1 <= r <= 2 * 109 
 *1 <= c <= 5
 *Rows are indexed from bottom to top and columns are indexed from left to right. 
 *
 *Output Format:
 *Output the answer in a single line.
 *
 *Sample Input:
 *6 3
 *
 *Sample Output:
 *25
 *
 *Explanation:
 *The number in the 6th row and 3rd column is 25.
 **/

public class GRID 
{
	public static void main(String[] args) 
	{
		@SuppressWarnings("resource")
		Scanner userInput = new Scanner(System.in);
	        
	    int row = userInput.nextInt();
	    int column = userInput.nextInt();
	        
	    if (row == 1) //Just to take it out of the way.
	    {
	       	if (column == 1)
	        	System.out.println("0");
	       	if (column == 2)
	        	System.out.println("2");
	       	if (column == 3)
	        	System.out.println("4");
	       	if (column == 4)
	        	System.out.println("6");
	       	if (column == 5)
	        	System.out.println("8");
	    }
        else
	    {  		        	
	       	if ((row) % 2 != 0) //For even rows this follows:
        	{
        		System.out.println(10*(((row+1)/2)-1) + 2*(column-1));
        	}
        	else				//For odd rows the same follows, but we add 1.
        	{
        		System.out.println((1 + 10*((row/2)-1) + 2*(column-1)));
        	}
	    }
	}
}
