import java.util.Scanner;

/**
 *@Author Ivan J. Miranda
 *STATUS: SOLVED! 
 * 
 *=FUNNY OR NOT=
 * 
 *Problem Statement:
 *Suppose you have a string S which has length N and is indexed from 0 to N-1. String R is the reverse of the string S. 
 *The string S is funny if the condition |Si - Si-1| = |Ri - Ri-1| is true for every i from 1 to N-1.
 *(Note: Given a string str, stri denotes the ascii value of the ith character (0-indexed) of str. |x| 
 *denotes the absolute value of an integer x)
 *
 *Input Format:
 *First line of the input will contain an integer T. T testcases follow. Each of the next T lines contains one string S. 
 *
 *Constraints:
 *1 <= T <= 10  
 *2 <= length of S <= 10000 
 *
 *Output Format:
 *For each string, print Funny or Not Funny in separate lines.
 *Sample Input:
 *2
 *acxz
 *bcxz
 *
 *Sample Output:
 *Funny
 *Not Funny
 *
 *Explanation:
 *Consider the 1st testcase acxz
 *here 
 *|c-a| = |x-z| = 2
 *|x-c| = |c-x| = 21
 *|z-x| = |a-c| = 2
 *Hence Funny.
 *
 *Consider the 2nd testcase bcxz
 *here
 *|c-b| != |x-z|
 *Hence Not Funny.
 **/

public class FunnyOrNot 
{
	public static void main(String[] args) 
	{
		@SuppressWarnings("resource")
		Scanner userInput = new Scanner(System.in);
		
		int t = Integer.parseInt(userInput.nextLine()); //t test cases follow.
				
		boolean funny;
		
		for (int i = 0; i < t; i++)
		{
			String str = userInput.nextLine(); //Reads line.
			funny = true; //Funny until proven otherwise.	
			
			for (int j = 0; j < str.length()-1 & funny; j++) //Java automatically tranforms characters into their
			{												 //ASCII values if arithmetic operators are used between them.
				if (!(Math.abs(str.charAt(j+1) - str.charAt(j)) == Math.abs(str.charAt((str.length()-1)-(j+1)) - str.charAt((str.length()-1)-(j)))))
					funny = false; //If there's a character that fails to meet the conditions, the string is not funny. 
			}
			
			if (funny)
				System.out.println("Funny");
			else
				System.out.println("Not Funny");
		}
	}
}
