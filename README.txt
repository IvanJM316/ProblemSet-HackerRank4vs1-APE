Welcome. These are some HackerRank problems which I solved
some time ago in a programming competition. They are somewhat
easy, but they were the first ones I solved ever. I will add
more contests soon. See source files for each problem's 
specifications.